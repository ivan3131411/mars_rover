require_relative '../src/mars_rover'

describe MarsRover do
  let(:landing_zone){'1,2,N'}

  it 'knows his landing zone' do
    commands = ''
    rover = MarsRover.new(landing_zone)
    
    rover.execute(commands)

    expect(rover.position).to eql(landing_zone)
  end

  it 'given the command M moves forward' do
    commands = 'M'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '1,3,N'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'given the comand L rotate direction by one to left' do
    commands = 'L'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '1,2,W'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'given the comand R rotate direction by one to right' do
    commands = 'R'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '1,2,E'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'given the comand R rotate direction by one to right' do
    commands = 'RR'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '1,2,S'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'rotates to rigth and move forward to that direction' do
    landing_zone = '1,2,N'
    commands = 'RM'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '2,2,E'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'rotates to left and move forward to that direction' do
    commands = 'LM'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '0,2,W'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'rotates to left twice and move forward to that direction' do
    commands = 'LLM'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '1,1,S'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'cant move if its at the limit of the board' do
    landing_zone = '0,0,N'
    commands = 'LM'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '0,0,W'
    expect(rover.position).to eql(expected_final_position)
  end

  it 'green belt test' do
    landing_zone = '0,0,N'
    commands = 'RMMMLMM'
    rover = MarsRover.new(landing_zone)

    rover.execute(commands)

    expected_final_position = '3,2,N'
    expect(rover.position).to eql(expected_final_position)
  end
end