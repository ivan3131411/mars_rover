class MarsRover
  def initialize(landing_position)
    @position = landing_position
  end

  def position
    @position
  end

  def execute(commands)
    commands.each_char do |command|
      if(command == 'M')
        move_forward
      elsif(command == 'R')
        change_direction_to('R')
      elsif(command == 'L')
        change_direction_to('L')
      end
    end
  end

  def move_forward
    if(direction == 'N' || direction == 'S')
      parsed_coordinates = position[2].to_i
      if(direction == 'N')
        new_coordinates = parsed_coordinates + 1
      elsif(direction == 'S')
        new_coordinates = parsed_coordinates - 1
      end

      return if new_coordinates < 0
      position[2] = new_coordinates.to_s
    end

    if(direction == 'E' || direction == 'W')
      parsed_coordinates = position[0].to_i

      if(direction == 'E')
        new_coordinates = parsed_coordinates + 1
      elsif(direction == 'W')
        new_coordinates = parsed_coordinates - 1
      end
      return if new_coordinates < 0
      position[0] = new_coordinates.to_s
    end
  end

  def change_direction_to(desired_direction)
    actual_direction_value = posible_directions[direction]

    if(desired_direction == 'L')
      actual_direction_value = actual_direction_value - 1
    elsif(desired_direction == 'R')
      actual_direction_value = actual_direction_value + 1
    end

    if(actual_direction_value.zero?)
      actual_direction_value = 4
    elsif(actual_direction_value == 5)
      actual_direction_value = 1
    end

    new_direction = posible_directions[actual_direction_value.to_s]
    position[4] = new_direction
  end

  def direction
    direction = position[4]

    direction
  end

  def posible_directions
    {
      'N' => 1,
      'E' => 2,
      'S' => 3,
      'W' => 4,
      '1' => 'N',
      '2' => 'E',
      '3' => 'S',
      '4' => 'W'
    }
  end
end
